﻿<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title>
        EServisCZ
    </title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    
    <?php include('header.php')?>
  
    <main>

        <h1>O nás</h1>

        <h3 class="aboutUs">Elektro servis EServisCZ</h3>

        <div class="oNas">

          <p>Jako společnost EServisCZ s.r.o. působíme na českém trhu od roku 2007.</p>
          <p>Vzhledem k dlouholetým zkušenostem v oblasti výpočetní techniky jsme získali široké spektrum zákazníků, díky nimž se dále rozvíjíme a poskytujeme více produktů od předních výrobců hardwaru a softwaru.</p>
          <p>Nabízíme také prodej a servis pro spotřební elektroniku, výpočetní a telekomunikační techniku, domácí elektrospotřebiče a veškeré příslušenství. Dále také řadu souvisejících služeb, jejichž cílem je co nejvíc usnadnit našim zákazníkům nákup i používání zakoupeného zboží, aby se z něj mohli dlouho a bezstarostně těšit.</p>
          <p>Opíráme se o několik základních hodnot, které definují celé naše působení. Jsou jimi flexibilita, férovost, jednoduchost, odbornost a vstřícnost.</p>
          <p>V rámci obchodní strategie se zaměřujeme zejména na špičkový zákaznický servis a služby.</p>
          <p>V našem sortimentu naleznete veškeré příslušenství k počítačům. Ale i spoustu elektroniky do domácnosti.</p>
          <br>
          <h4>Zákazníci a naše směřování</h4>
          <p>Mezi naše zákazníky patří nejen domácí uživatelé, hráči a počítačoví nadšenci, ale také firmy, školy a instituce.</p>
          <p>Zabýváme se návrhem a realizací počítačových sítí domácností i firem.</p>
          <p>Vytváříme řešení na klíč, stejně jako úpravy a upgrady stávajících stavů.</p>
          <p>Jsme zaměřeni i na trend trvalé udržitelnosti, který plně a rádi sdílíme s dlouhodobými direktivami EU a proto i my se snažíme přispět svým dílem a nabízíme jako službu zpětný odběr a likvidaci přístrojů, které dále necháváme zlikvidovat či alespoň částečně znovu využít v rámci správných postupů.</p>
          <br>
          <h4>Sídlo společnosti EServisCZ</h4>
          <div>
          <a href="contacts.php"><img class="sidlo" src="obr1.jpg" alt="sídlo"></a>
          <a href="contacts.php"><img class="sidlo" src="obr2.jpg" alt="sídlo"></a>
          </div>
          <p></p>
        
        </div>

        <br>
    
    </main>

    <footer>
    <div class="container">
        <p>© 2021 EServisCZ s.r.o. All rights reserved</p>
    </div>
    </footer>
    

  </body>
</html>
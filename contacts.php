﻿<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title>
        EServisCZ
    </title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    
    <?php include('header.php')?>
  
    <main>
    
            <h1>Kontakty</h1>
                    <br>
                    <br>

                      <table class="contactTable">
                          <tr class="nameTable"><th>Servis</th><td></td></tr>
                          <tr><th>Název</th><td>EServisCZ s.r.o.</td></tr>
                          <tr><th>IČO</th><td>25874196</td></tr>
                          <tr><th>DIČ</th><td>CZ25874196</td></tr>
                          <tr><th>Město</th><td>Čelákovice</td></tr>
                          <tr><th>PSČ</th><td>250 88</td></tr>
                          <tr><th>Adresa</th><td>Rybářská 154</td></tr>
                          <tr><th>Telefon</th><td>741 852 963</td></tr>
                          <tr><th>E-mail</th><td>info@eserviscz.cz</td></tr>
                          <tr><th>Kontakt</th><td>Pavel Tepličanec</td></tr>
                      </table>
                    
                    <br>
                    <br>
                    
                    <h3 class="aboutUs">Kde nás najdete</h3>

                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2625.7152198051363!2d14.747748715611978!3d50.164329715820955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x470bf6afe454d4cd%3A0xb32f422c5b043ae7!2zUnliw6HFmXNrw6EgMTU0LzE5LCAyNTAgODggxIxlbMOha292aWNl!5e1!3m2!1scs!2scz!4v1611001288866!5m2!1scs!2scz" width="900" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

                    <div class="form">
                        <h3>Můžete nám napsat</h3>
                        <form method="post" action="">
                            <input type="text" name="name" placeholder="Jméno" required>
                            <input type="email" name="email" placeholder="Email" required>
                            <textarea name="text" placeholder="Text"></textarea>
                            <input type="submit" name="submit" value="odeslat" class="btn">
                        </form>
                    </div>

                    <br>
                    
    </main>  

    <footer>
    
        <p>© 2021 EServisCZ s.r.o. All rights reserved</p>
    
    </footer>

  </body>
</html>
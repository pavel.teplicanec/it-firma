﻿<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title>
        EServisCZ
    </title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    

    <?php include('header.php')?>
    
    <main>

        <h1>EServisCZ</h1>

        <img class="intro" src="uvod.gif" alt="uvodni obrazek">
        
        <div class="menu2">
            <ul>
                  <li>

                    <h3> Servis </h3>

                  </li>
                  
                    <li><a>Stolní Počítače</a></li>
                    <li><a>Notebooky</a></li>
                    <li><a>Telefony</a></li>
                    <li><a>Konzole</a></li>
                    <li><a>Domácí spotřebiče</a></li>

            </ul>
        </div>

        <div class="menu2" style="padding-top: 1px">
            <ul>
                    <li>
    
                      <h3> Prodej </h3>

                    </li>

                    <li><a>Stolní Počítače</a></li>
                    <li><a>Notebooky</a></li>
                    <li><a>Telefony</a></li>
                    <li><a>Konzole</a></li>
                    <li><a>Televize</a></li>
                    <li><a>Komponenty</a></li>
                    <li><a>Příslušenství</a></li>
    
            </ul>
        </div>

        <div class="services">
        
            <article class="info">
              <h2>Servis</h2>
              <a href="services.php"><img src="servis.jpg" alt="servis"></a>
            </article>
           
            <article class="info">
              <h2>Prodej</h2>
              <a href="services.php"><img src="prodej.jpg" alt="prodej"></a>
            </article>
           
            <article class="info">
              <h2>Sestavy</h2>
              <a href="services.php"><img src="sestava.jpg" alt="sestavy"></a>
            </article>
        
        </div>

        <br>
        <br>
    
    </main>
    
    <footer>
    
        <p>© 2021 EServisCZ s.r.o. All rights reserved</p>
    
    </footer>
    

  </body>
</html>
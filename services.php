﻿<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title>
        EServisCZ
    </title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    
    <?php include('header.php')?>
  
    <main>
      
      <h1>Služby</h1>

      
        
            <article class="info2">
              <h2>Servis</h2>
              <p>Provádíme záruční i pozáruční opravy větších i malých elektrospotřebičů.</p>
              <p>Servisujeme stolní počítače, notebooky, tablety, telefony, konzole nebo i navigace.</p>
              <p>Opravujeme i různé počítačové příslušenství. Klávesnice a myši, monitory nebo také tiskárny.</p>
              <p>Jsme schopni opravit i některé jednotlivé komponenty vašich počítačů či notebooků.</p>
              <p>Řešení softwarových chyb a závad, nebo i přehrávání dat a výměna disku.</p>
              <p>Provádíme také úpravy a upgrade počítačů nebo notebooků.</p>
              <img class="servis2" src="servis2.jpg" alt="servis">
              <p>Fyzické i softwarové čištění zařízení.</p>
              <p>Je možné si u nás nechat opravit i fyzicky poškozené notebooky.</p>
              <p>Umíme si poradit i s nefunkční nebo jakkoliv problémovu herní konzolí.</p>
              <p>Vyměňujeme vadné nebo rozbité displeje telefonů a tabletů nebo i jejich baterie. Opravíme i nefunkční vstup pro nabíjení.</p>
              <p>Můžete si u nás nechat nalepit ochrannou fólii nebo sklo na svůj nový telefon.</p>
              <p>Máme možnost aktualizovat vši navigaci pokud již nemá aktuální.</p>
              <p>Instalace a nastavování domácích sítí u vás doma.</p>
              <p>Ceny oprav a služeb se liší závažností, časem a cenou komponentů.</p>
            </article>
           
            <article class="info2">
              <h2>Prodej</h2>
              <p>V EServisCZ nejen opravjeme, ale také prodáváme spoustu kvalitního a spolehlivého elektra od předních výrobců.</p>
              <p>Prodáváme kompletní domácí, pracovní a kancelářské, all in one nebo herní počítače.</p>
              <p>Veškeré příslušenství i jednotlivé komponenty pro náročnější zákazníky.</p>
              <p>Nabízíme všechny druhy notebooků.</p>
              <img class="prodej2" src="prodej2.jpg" alt="prodej">
              <p>Standardní i pracovní, také herní, 2v1 nebo i MacBook</p>
              <p>V naší nabídce máme i menší elektroniku jako třeba telefony, tablety, hodinky, navigace a spoustu dalšího.</p>
              <p>Telefony Smartphone s Android nebo IOS, Fotomobily, Odolné mobily nebo tlačítkové pro seniory</p>
              <img class="prodej3" src="prodej3.jpg" alt="prodej">
              <p>Naleznete zde i spoustu elektrospotřebičů do vaší domácnosti.</p>
              <p>Od našeho servisu si můžete objednat i dovoz.</p>
            </article>
           
            <article class="info2">
              <h2>Sestavy</h2>
              <p>Stavíme kompletní počítačové sestavy dle přání a možností zákazníka.</p>
              <img class="sestava2" src="sestava2.jpg" alt="sestava">
              <p>Sestavte si počítač svých snů. Stačí si vybrat komponenty jaké chcete. A mi vám počítač složíme a nastavíme.</p>
              <p>Pokud nevíte co vybrat, nezoufejte s výběrem vám poradíme.</p>
              <p>Máme i kompletní sestavy s perfektním poměrem ceny a výkonu.</p>
              <p>Vyberete si ať už hledáte počítače na hraní nebo do kanceláře či domácnosti.</p>
              <img class="sestava3" src="sestava3.jpg" alt="sestava">
              <p>Chtěli by jste jinou grafiku, disk a nebo RAMky? Není problém.</p>
              <p>Doporučujeme kompletní sestavy s procesory Intel a operačním systémem Windows.</p>
              <p>Je možnost si domluvit i splátkový kalendář.</p>
            </article>
        
        

        <br>
    
    </main>
    
    <footer>
    <div class="container">
        <p>© 2021 EServisCZ s.r.o. All rights reserved</p>
    </div>
    </footer>
    

  </body>
</html>